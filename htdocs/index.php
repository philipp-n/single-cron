<?php

if (empty($_POST)) {
    ?>

    <form action="" method="post">

        <label for="philipp@">Philipp</label>
        <input type="checkbox" id="philipp@" name="philipp@" checked="checked"> |
        <label for="vera@">Vera</label>
        <input type="checkbox" id="vera@" name="vera@"> |
        <label for="natinhopes@">Natalja (natinhopes)</label>
        <input type="checkbox" id="natinhopes@" name="natinhopes@"> |
        <label for="nsao@">Natalja (nsao)</label>
        <input type="checkbox" id="nsao@" name="nsao@">
        <br><br>

        <label for="when">Datetime</label><br>
        <input type="text" id="when" name="when" placeholder="09:00 18 Nov 2022" required><br>
        <label for="message">Last name:</label><br>
        <input type="text" id="message" name="message" placeholder="Notification message" required><br>
        <label for="add-year"> Notify only once</label><br>
        <input type="checkbox" id="add-year" name="add-year" checked="checked"><br><br>

        <input type="submit" value="Save">
    </form>

    <?php return; ?>

<?php } else { ?>

    <button onclick="window.location.href = window.location.href;">Reload page</button>

    <h1>Notification saved: </h1>
    <p><?= DateTime::createFromFormat("H:i d M Y", $_POST['when'])->format("Y-m-d H:i:s") ?></p>
    <p><?= $_POST['message'] ?></p>

<?php } ?>

<?php

$config = json_decode(file_get_contents(__DIR__ . "/config/main.json"));

require_once "{$config->mainDirLocation}vendor/autoload.php";

use PhilippN\SingleCron\CronScheduleExpressionGenerator;

$tasksFilePath = $config->mainDirLocation . "tasks/ui.json";

$tasks = [];

if (file_exists($tasksFilePath)) {
    $tasks = json_decode(file_get_contents($tasksFilePath), true);
}

foreach (['philipp@', 'vera@', 'natinhopes@', 'nsao@'] as $emailRecipient) {
    if (isset($_POST[$emailRecipient])) {
        $tasks['tasks'][date("YmdHis")][] = [
            'cron_expression' => (new CronScheduleExpressionGenerator(
                DateTime::createFromFormat("H:i d M Y", $_POST['when'])
            ))->generate(isset($_POST['add-year'])),
            'exec' => [
                '{$php} {$sendEmailScript} {$@to}{$' . $emailRecipient . '} --subject="' . $_POST['message'] . '" {$@from}' => []
            ]
        ];
    }
}

file_put_contents($tasksFilePath, json_encode($tasks, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
?>

<h3>Tasks</h3>


<pre><?= json_encode($tasks, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) ?></pre>
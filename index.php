<?php

if (php_sapi_name() !== "cli") {
    throw new Exception("Only CLI usage is allowed");
}

require_once __DIR__ . "/vendor/autoload.php";

use PhilippN\SingleCron\TasksCaller;
use ProPhp\Filesystem\Files;
use ProPhp\Filesystem\FilesParams;

$files = Files::list(__DIR__ . "/tasks", (new FilesParams())->includedFilenamePatterns(['*.json']));

(new TasksCaller(
    array_merge([ __DIR__ . "/tasks.h.json"], $files)
))->execute();

<?php

if (php_sapi_name() !== "cli") {
    throw new Exception("Only CLI usage is allowed");
}

$rootPath = dirname(__DIR__);

require_once "$rootPath/vendor/autoload.php";

use ProPhp\Filesystem\Files;
use ProPhp\Filesystem\FilesParams;
use HJSON\HJSONParser;
use PhilippN\SingleCron\CronScheduleExpression;

$files = array_merge(
    ["$rootPath/tasks.h.json"],
    Files::list("$rootPath/tasks", (new FilesParams())->includedFilenamePatterns(['*.json']))
);


foreach ($files as $file) {
    $data = (new HJSONParser())
        ->parse(file_get_contents($file), ['assoc' => true]);


    $processedTasks = $data['tasks'] ?? [];

    foreach ($processedTasks as $tasksGroupTitle => &$tasksGroup) {
        foreach ($tasksGroup as $index => $task) {

            $cronExpressionParamCount = count(explode(" ", $task['cron_expression']));
            if ($cronExpressionParamCount > 5) {

                if (
                    (new CronScheduleExpression($task['cron_expression']))->toDateTime() < new \DateTime
                ) {
                    unset($tasksGroup[$index]);
                }

            }
        }
    }

    $processedTasks = array_filter($processedTasks, function ($item) {
        return !empty($item);
    });

    $data['tasks'] = array_values($processedTasks);

    file_put_contents($file, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}
<?php

namespace PhilippN\SingleCron;

class CronScheduleExpression
{
    private string $expression;

    public function __construct(string $expression)
    {
        $this->expression = $expression;
    }

    private function validateNumericParameter(string $input, string $parameter): bool
    {
        return (int)$parameter === (int)$input;
    }

    private function validateStepParameter(string $input, string $parameter): bool
    {
        $number = (int)substr($parameter, 2);
        return $input % $number == 0;
    }

    private function validateRangeParameter(string $input, string $parameter, string $parameterTitle = null): bool
    {
        $exploded = explode("-", $parameter);
        if (count($exploded) !== 2) {
            throw new \Exception(
                "Wrong range value '$parameter' ($parameterTitle) in cron expression '{$this->expression}'"
            );
        }
        list($from, $to) = $exploded;

        $rangeIsValid = false;

        foreach (range((int)$from, (int)$to) as $rangeSubValue) {
            if ($rangeSubValue === (int)$input) {
                $rangeIsValid = true;
            }
        }

        return $rangeIsValid;
    }

    private function validateExplodedExpression(array $explodedExpression)
    {
        foreach ($explodedExpression as $item) {
            if (strlen(trim($item)) === 0) {
                // @todo Must be tested
                throw new \Exception("Invalid cron expression '{$this->expression}'. Check extra spaces etc.");
            }
        }
    }

    private function prepareCronSettings(): array
    {

        $explodedExpression = explode(" ", $this->expression);

        $this->validateExplodedExpression($explodedExpression);

        if (count($explodedExpression) === 5) {

            list($_minute, $_hour, $_monthDay, $_month, $_weekDay) = $explodedExpression;

            $cronSettings = array_combine(
                ['minute', 'hour', 'monthDay', 'month', 'weekDay'],
                [$_minute, $_hour, $_monthDay, $_month, $_weekDay]
            );

        } elseif (count($explodedExpression) === 6) {

            list($_minute, $_hour, $_monthDay, $_month, $_weekDay, $_year) = $explodedExpression;

            $cronSettings = array_combine(
                ['minute', 'hour', 'monthDay', 'month', 'weekDay', 'year'],
                [$_minute, $_hour, $_monthDay, $_month, $_weekDay, $_year]
            );

        } elseif (count($explodedExpression) === 7) {

            list($_second, $_minute, $_hour, $_monthDay, $_month, $_weekDay, $_year) = $explodedExpression;

            $cronSettings = array_combine(
                ['second', 'minute', 'hour', 'monthDay', 'month', 'weekDay', 'year'],
                [$_second, $_minute, $_hour, $_monthDay, $_month, $_weekDay, $_year]
            );

        } else {
            // @todo Must be tested + DRY
            throw new \Exception("Invalid cron expression '{$this->expression}'. Check extra spaces etc.");
        }

        return $cronSettings;
    }

    // @todo More laconic implementation using array_pad()
    private function prepareDateTimeParameters(\DateTime $datetime): array
    {
        $explodedExpression = explode(" ", $this->expression);

        $this->validateExplodedExpression($explodedExpression);

        if (count($explodedExpression) === 5) {

            list($minute, $hour, $monthDay, $month, $weekDay) = explode(" ", $datetime->format("i H j n N"));

            $DateTimeParameters = array_combine(
                ['minute', 'hour', 'monthDay', 'month', 'weekDay'],
                [$minute, $hour, $monthDay, $month, $weekDay]
            );

        } elseif (count($explodedExpression) === 6) {

            list($minute, $hour, $monthDay, $month, $weekDay, $year) =
                explode(" ", $datetime->format("i H j n N Y"));

            $DateTimeParameters = array_combine(
                ['minute', 'hour', 'monthDay', 'month', 'weekDay', 'year'],
                [$minute, $hour, $monthDay, $month, $weekDay, $year]
            );

        } elseif (count($explodedExpression) === 7) {

            list($second, $minute, $hour, $monthDay, $month, $weekDay, $year) =
                explode(" ", $datetime->format("s i H j n N Y"));

            $DateTimeParameters = array_combine(
                ['second', 'minute', 'hour', 'monthDay', 'month', 'weekDay', 'year'],
                [$second, $minute, $hour, $monthDay, $month, $weekDay, $year]
            );

        } else {
            // @todo Must be tested + DRY
            throw new \Exception("Invalid cron expression '{$this->expression}'. Check extra spaces etc.");
        }

        return $DateTimeParameters;
    }

    public function isMatcingDatetime(\DateTime $datetime = null): bool
    {
        $datetime = $datetime ?? new \DateTime;

        $cronSettings = $this->prepareCronSettings();
        $actualDateTime = $this->prepareDateTimeParameters($datetime);

        // @todo DRY (Use recursion ???)
        foreach ($cronSettings as $key => $value) {
            if ($value === '*') {
                continue;
            } elseif (str_contains($value, ",")) {
                $isValid = false;

                foreach (explode(",", $value) as $subValue) {

                    if (is_numeric($subValue)) {

                        if ($this->validateNumericParameter($actualDateTime[$key], $subValue)) {
                            $isValid = true;
                        }

                    } elseif (str_starts_with($subValue, "*/")) {

                        if ($this->validateStepParameter($actualDateTime[$key], $subValue)) {
                            $isValid = true;
                        }

                    } elseif (str_contains($value, "-")) {

                        if ($this->validateRangeParameter($actualDateTime[$key], $subValue, $key)) {
                            $isValid = true;
                        }

                    } else {

                        $this->throwException($subValue, $key);

                    }
                }

                if ($isValid === false) {
                    return false;
                }

            } elseif (is_numeric($value)) {

                if (!$this->validateNumericParameter($actualDateTime[$key], $value)) {
                    return false;
                }

            } elseif (str_starts_with($value, "*/")) {

                if (!$this->validateStepParameter($actualDateTime[$key], $value)) {
                    return false;
                }

            } elseif (str_contains($value, "-")) {

                if (!$this->validateRangeParameter($actualDateTime[$key], $value, $key)) {
                    return false;
                }

            } else {

                $this->throwException($subValue, $key);

            }

        }

        return true;
    }

    private function throwException(string $expressionParameterValue, string $expressionParameterTitle)
    {
        throw new \Exception(
            "Unknown cron expression '$expressionParameterValue' ($expressionParameterTitle) in '{$this->expression}'"
        );
    }

    public function toDateTime(): ?\DateTime
    {
        $cs = $this->prepareCronSettings();

        $month = str_pad($cs['month'], 2, 0, STR_PAD_LEFT);
        $monthDay = str_pad($cs['monthDay'], 2, 0, STR_PAD_LEFT);
        $hour = str_pad($cs['hour'], 2, 0, STR_PAD_LEFT);
        $minute = str_pad($cs['minute'], 2, 0, STR_PAD_LEFT);

        switch (count($cs)) {
            case 5:
                return \DateTime::createFromFormat(
                    "YmdHi",
                    "2000$month$monthDay$hour$minute"
                );
            case 6:
                return \DateTime::createFromFormat(
                    "YmdHi",
                    "{$cs['year']}$month$monthDay$hour$minute"
                );
            case 7:
                $second = str_pad($cs['second'], 2, 0, STR_PAD_LEFT);
                return \DateTime::createFromFormat(
                    "YmdHis",
                    "{$cs['year']}$month$monthDay$hour$minute$second"
                );
        }

        return null;
    }
}
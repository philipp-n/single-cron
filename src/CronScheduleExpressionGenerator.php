<?php

namespace PhilippN\SingleCron;

class CronScheduleExpressionGenerator
{
    private \DateTime $DateTime;

    public function __construct(\DateTime $DateTime)
    {
        $this->DateTime = $DateTime;
    }

    public function generate(bool $addYear = false): string
    {
        // 'minute', 'hour', 'monthDay', 'month', 'weekDay', ('year')
        $expression =
            (int)$this->DateTime->format("i") . " " .
            (int)$this->DateTime->format("H") . " " .
            (int)$this->DateTime->format("d") . " " .
            (int)$this->DateTime->format("m") . " " .
            "*";

        if ($addYear) {
            $expression .= " " . (int)$this->DateTime->format("Y");
        }

        return $expression;
    }
}
<?php

namespace PhilippN\SingleCron;

class TaskCaller
{
    public static function file_get_contents(string $url, bool $logExecution = true)
    {
        if ($logExecution) {
            self::log('HTTP', $url);
        }

        $context = stream_context_create(['http' => ['timeout' => 0.01]]);
        /** Do not wait for execution (only 0.01 seconds), is executed on the background */
        return file_get_contents($url, null, $context);
    }

    public static function exec(string $execString, bool $logExecution = true)
    {
        if ($logExecution) {
            self::log('EXEC', $execString);
        }

        /** Do not wait for execution, is executed on the background */
        exec("$execString > /dev/null 2>&1 &");
    }

    private static function log(string $type, string $msg)
    {
        file_put_contents(
            dirname(__DIR__) . "/logs/main.log",
            (new \DateTime)->format("Y-m-d H:i:s") . " $type '$msg'\n",
            FILE_APPEND
        );
    }
}


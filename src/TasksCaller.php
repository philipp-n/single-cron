<?php

namespace PhilippN\SingleCron;

use HJSON\HJSONParser;

class TasksCaller
{
    private $DateTime;
    private $data;

    public function __construct(string|array $dataFilePath_s, \DateTime $DateTime = null)
    {
        $this->DateTime = $DateTime ?? new \DateTime;

        $data = is_string($dataFilePath_s) ?
            (new HJSONParser())->parse(file_get_contents($dataFilePath_s), ['assoc' => true]) :
            $this->mergeData($dataFilePath_s);

        $this->data = $data;
    }

    private function mergeData(array $dataFilePath_s): array
    {
        $result = [];
        $HJSONParser = new HJSONParser();
        $i = 1;

        foreach ($dataFilePath_s as $dataFilePath) {

            $subData = $HJSONParser->parse(file_get_contents($dataFilePath), ['assoc' => true]);

            if ($i === 1) {
                $result = array_merge_recursive(
                    $result,
                    $subData
                );
                $i++;
                continue;
            }

            if (isset($subData['variables'])) {
                throw new \Exception(
                    "Variables cannot be set in config sub-files: " .
                    "main '{$dataFilePath_s[0]}', current config '$dataFilePath'"
                );
            }

            $result = array_merge_recursive(
                $result,
                $subData
            );
        }

        return $result;
    }

    private function getTasks()
    {
        return $this->data['tasks'] ?? [];
    }

    private function getVariables()
    {
        return $this->data['variables'] ?? [];
    }

    private function replaceVariables(string $task): string
    {
        while (str_contains($task, "{\$")) {

            $variableStartPos = strpos($task, "{\$");
            $variableEndPos = strpos($task, "}");
            $variable = substr($task, $variableStartPos, $variableEndPos - $variableStartPos + 1);
            $variableTitle = substr($variable, 2, -1);
            if (!isset($this->getVariables()[$variableTitle])) {
                throw new \Exception("Variable '$variableTitle' is missing");
            }
            $task = str_replace($variable, $this->getVariables()[$variableTitle], $task);
        }

        return $task;
    }

    public function execute()
    {
        foreach ($this->getTasks() as $taskGroupLabel => $tasksData) {

            foreach ($tasksData as $taskData) {
                if ((new CronScheduleExpression($taskData['cron_expression']))->isMatcingDatetime($this->DateTime)) {

                    $logExecution = !($taskGroupLabel === 'maintenance scripts' || $taskData['cron_expression'] === '* * * * *');

                    foreach ($taskData['exec'] ?? [] as $task => $taskSettings) {
                        TaskCaller::exec($this->replaceVariables($task), $logExecution);
                    }
                    foreach ($taskData['file_get_contents'] ?? [] as $task => $taskSettings) {
                        TaskCaller::file_get_contents($this->replaceVariables($task), $logExecution);
                    }
                }
            }

        }
    }
}
<?php

require_once dirname(__DIR__) . "/vendor/autoload.php";

use HJSON\HJSONParser;
use PhilippN\SingleCron\CronScheduleExpressionGenerator;

$useDatetimeModifier = function (DateTime $DateTime, string $modifierTitle, int|string $modifierValue) {
    switch ($modifierTitle) {
        case "hours":
            $DateTime->setTime((int)$modifierValue, (int)$DateTime->format("i"));
            break;
        case "minutes":
            $DateTime->setTime((int)$DateTime->format("H"), (int)$modifierValue);
            break;
        case "days":
            $DateTime->modify("$modifierValue days");
            break;
        default:
            throw new Exception("Unknown DateTime modifier '$modifierTitle'");
    }
};


$prepareCronExpressions = function (string $birthdayDate, array $datetimeModifiers) use ($useDatetimeModifier) {

    $initialDateTime = DateTime::createFromFormat("d M", $birthdayDate)->setTime(0, 0);

    $cronExpressions = [];

    foreach ($datetimeModifiers as $modifierSet) {
        $currentDateTime = clone $initialDateTime;
        foreach ($modifierSet as $modifierTitle => $modifierValue) {
            $useDatetimeModifier($currentDateTime, $modifierTitle, $modifierValue);
        }

        $cronExpressions[] = (new CronScheduleExpressionGenerator($currentDateTime))->generate();
    }

    return $cronExpressions;
};

$configData = (new HJSONParser())
    ->parse(file_get_contents(dirname(__DIR__) . "/config/main.h.json"), ['assoc' => true]);

$inputData = (new HJSONParser())
    ->parse(file_get_contents(__DIR__ . "/input/birthdays.h.json"), ['assoc' => true]);

$result = [
    'tasks' => [],
];

foreach ($inputData as $groupTitle => $groupData) {
    foreach ($groupData['people'] as $person => $birthdayDate) {

        $messageConfig = [];

        if (is_array($birthdayDate)) {
            list($birthdayDate, $messageConfig) = $birthdayDate;
        }

        $cronExpressions = $prepareCronExpressions($birthdayDate, $groupData['config']['datetime modifiers']);

        if (
            isset($groupData['config']['disable main recipient']) &&
            $groupData['config']['disable main recipient'] === true
        ) {
            if (empty($groupData['config']['additional email recipients'])) {
                throw new Exception(
                    "Main recipient cannot be disabled if no additional recipients are specified! " .
                    "Group title: $groupTitle"
                );
            }

            $emailRecipients = [];

        } else {

            $emailRecipients = [$configData['main email recipient']];

        }


        if (isset($groupData['config']['additional email recipients'])) {
            $emailRecipients = array_merge($emailRecipients, $groupData['config']['additional email recipients']);
        }

        $emailRecipientsString = implode(",", $emailRecipients);

        $russianSpeakingRecipientsIncluded = false;

        foreach ($configData['russian speaking email recipients'] as $russianSpeakingRecipient) {
            if (in_array($russianSpeakingRecipient, $emailRecipients)) {
                $russianSpeakingRecipientsIncluded = true;
            }
        }


        if ($russianSpeakingRecipientsIncluded) {

            $message = isset($messageConfig['message']) && empty($messageConfig['message']) ?
                "$person - $birthdayDate" :
                "День рождения $person - $birthdayDate";

            foreach ($cronExpressions as $cronExpression) {
                $result['tasks']["$person's birthday"][] = [
                    'cron_expression' => $cronExpression,
                    "exec" => [
                        "{\$php} {\$sendEmailScript} " .
                        "--recipients=$emailRecipientsString " .
                        "--subject=\"$message\" " .
                        "--from=\"Авто Напоминание <notifications@nikolajev.ee>\" " .
                        "--message=\"$message\"" => []
                    ]
                ];
            }

        } else {

            $message = isset($messageConfig['message']) && empty($messageConfig['message']) ?
                "$person - $birthdayDate" :
                "$person's birthday is on $birthdayDate";

            foreach ($cronExpressions as $cronExpression) {
                $result['tasks']["$person's birthday"][] = [
                    'cron_expression' => $cronExpression,
                    "exec" => [
                        "{\$php} {\$sendEmailScript} " .
                        "--recipients=$emailRecipientsString " .
                        "--subject=\"$message\" " .
                        "--from=\"Auto Notification <notifications@nikolajev.ee>\" " .
                        "--message=\"$message\"" => []
                    ]
                ];
            }

        }

    }
}

file_put_contents(
    dirname(__DIR__) . "/tasks/birthdays.h.json",
    json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
);
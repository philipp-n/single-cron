<?php

use ProPhp\TestsLite\Helper;

use HJSON\HJSONParser;
use PhilippN\SingleCron\CronScheduleExpression;

$location = Helper::getLocation();

$inputRaw = Helper::getInputData($location, 'json');
$input = (new HJSONParser())->parse($inputRaw, ['assoc' => true]);

$actualOutput = [];

foreach ($input as $row) {
    list($expression, $datetime, $isValid) = $row;
    $actual = (new CronScheduleExpression($expression))->isMatcingDatetime(new \DateTime($datetime));
    // @todo Helper updated (upload)
    Helper::assertEquals($actual, $isValid, json_encode($row));
}


<?php

use ProPhp\TestsLite\Helper;

use PhilippN\SingleCron\TaskCaller;


$outputFilePath = "/var/www/html/tests/tmp/output.txt";
if(file_exists($outputFilePath)){
    exec("rm '$outputFilePath'");
}

TaskCaller::exec("php /var/www/html/tests/tasks/write-to-file.php");

Helper::assertEquals(file_exists($outputFilePath), false);

sleep(2);

Helper::assertEquals(file_exists($outputFilePath), true);
<?php

use ProPhp\TestsLite\Helper;

use PhilippN\SingleCron\TaskCaller;


$outputFilePath = "/var/www/html/tests/tmp/output.txt";
if(file_exists($outputFilePath)){
    exec("rm '$outputFilePath'");
}

$instantOutput = TaskCaller::file_get_contents("http://localhost/tests/tasks/write-to-file.php");

Helper::assertEquals($instantOutput, "Hello world");

Helper::assertEquals(file_exists($outputFilePath), false);

sleep(1);

Helper::assertEquals(file_exists($outputFilePath), true);
<?php

use ProPhp\TestsLite\Helper;

use PhilippN\SingleCron\TasksCaller;


# exec

$outputFilePath = "/var/www/html/tests/tmp/output.txt";
if(file_exists($outputFilePath)){
    exec("rm '$outputFilePath'");
}

$location = Helper::getLocation();

$TasksCaller = new TasksCaller(
    [
        Helper::getDataDirPath($location) . "/tasks.main.h.json",
        Helper::getDataDirPath($location) . "/tasks.h.json",
    ],
    new DateTime("2022-12-31 23:59:00")
);

$TasksCaller->execute();

Helper::assertEquals(file_exists($outputFilePath), false);

sleep(2);

Helper::assertEquals(file_exists($outputFilePath), true);


# file_get_contents

exec("rm '$outputFilePath'");


$TasksCaller = new TasksCaller(
    [
        Helper::getDataDirPath($location) . "/tasks.main.h.json",
        Helper::getDataDirPath($location) . "/tasks.h.json",
    ],
    new DateTime("2022-12-31 23:58:00")
);

$TasksCaller->execute();

Helper::assertEquals(file_exists($outputFilePath), false);

sleep(1);

Helper::assertEquals(file_exists($outputFilePath), true);